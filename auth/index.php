<?php
require_once '../lib/auth.php';
require_once '../header.php';
?>
<?php if (!isset($_SESSION['sessid'])) :?>
	<div class="text-center">
		<form class="form-signin" method="post" action="">
			<?if ($errorAuth) :?>
				<div class="alert-danger"><?php echo $errorMessage?></div>
			<?endif;?>
			<h1 class="h3 mb-3 font-weight-normal">Пожалуйста, войдите</h1>
			<label for="inputLogin" class="sr-only">Логин</label>
			<input type="login" id="inputLogin" name="login" class="form-control" placeholder="Логин" required="" autofocus="">
			<label for="inputPassword" class="sr-only">Пароль</label>
			<input type="password" id="inputPassword" name="password" class="form-control" placeholder="Пароль" required="">
			<button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
		</form>
	</div>
<?php else :?>
	<div class="text-center">
		<form class="form-signin" method="post" action="">
			<h1 class="h3 mb-3 font-weight-normal">Вход выполнен, <?php echo $_SESSION['name']?></h1>
			<p>Ваш город: <?php echo $_SESSION['city']?></p>
			<p><a href="../order/">Создать заявку</a></p>
			<p><a href="../review/">Оставить отзыв</a></p>
			<p><a href="../lib/exit.php">Выйти</a></p>
		</form>
	</div>
<?php endif;?>
<?php require_once '../footer.php'?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script type="text/javascript" src='js/validation.js'></script>