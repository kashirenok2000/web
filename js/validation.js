$(document).ready(function() {
    $("#order-form").validate({
        rules : {
            name : {
                required: true
            },
            surname : {
                required: true
            },
            email : {
                required: true
            },
            address : {
                required: true
            },
            city : {
                required: true
            },
            good : {
                required: true
            }
        },
        messages : {
            name : {
                required: "<span class='text-danger'>* необходимо заполнить</span>"
            },
            surname : {
                required: "<span class='text-danger'>* необходимо заполнить</span>"
            },
            email : {
                required: "<span class='text-danger'>* необходимо заполнить</span>"
            },
            address : {
                required: "<span class='text-danger'>* необходимо заполнить</span>"
            },
            city : {
                required: "<span class='text-danger'>* необходимо заполнить</span>"
            },
            good : {
                required: "<span class='text-danger'>* необходимо заполнить</span>"
            }
        }
    });

});