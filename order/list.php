<?php 
require_once '../lib/order.php';
require_once '../header.php';
$orders = getOrders();
?>
<div class="col-12 order-md-last">
    <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-muted">Последние заявки</span>
        <span class="badge bg-secondary rounded-pill"><a href="./" style="color: white;">Оставить заявку</a></span>
    </h4>
    <ul class="list-group mb-3">
        <?php foreach ($orders as $order) :?>
            <li class="list-group-item d-flex justify-content-between lh-sm">
                <div>
                <h6 class="my-0"><?php echo $order['name'] . ' ' . $order['surname']?></h6>
                <small class="text-muted"><?php echo $order['email']?></small><br>
                <small class="text-muted"><?php echo $cities[$order['city']] . ', ' . $order['address']?></small><br>
                </div>
                <span class="text-muted"><?php echo $goods[$order['good']]?></span>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php require_once '../footer.php'?>