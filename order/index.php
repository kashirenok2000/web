<?php 
require_once '../lib/order.php';
if (isset($_POST) && $_POST)
{
    $data = [];
    if (isset($_POST['name'])) $data['name'] = $_POST['name'];
    if (isset($_POST['surname'])) $data['surname'] = $_POST['surname'];
    if (isset($_POST['address'])) $data['address'] = $_POST['address'];
    if (isset($_POST['city'])) $data['city'] = $_POST['city'];
    if (isset($_POST['email'])) $data['email'] = $_POST['email'];
    if (isset($_POST['good'])) $data['good'] = $_POST['good'];
    $result = addOrder($data);
    if ($result)
    {
        $host = $_SERVER['HTTP_HOST'];
        $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        header('Location: http://' . $host . $uri);
    }
}
require_once '../header.php';
$orders = getOrders(5);
?>
<div class="row g-3">
    <div class="col-md-6 col-lg-6">
        <h4 class="mb-2">Новая заявка</h4>
        <form class="needs-validation" id="order-form" method="POST" action="">
            <div class="row g-3">
                <div class="col-sm-6">
                    <label for="firstName" class="form-label">Имя</label>
                    <label id="firstName-error" class="error" for="firstName"></label>
                    <input type="text" class="form-control" id="firstName" name="name">
                </div>

                <div class="col-sm-6">
                    <label for="lastName" class="form-label">Фамилия</label>
                    <label id="lastName-error" class="error" for="lastName"></label>
                    <input type="text" class="form-control" id="lastName" name="surname">
                </div>

                <div class="col-12">
                    <label for="email" class="form-label">Email</label>
                    <label id="email-error" class="error" for="email"></label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>

                <div class="col-12">
                    <label for="address" class="form-label">Адрес</label>
                    <label id="address-error" class="error" for="address"></label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>

                <div class="col-12 mt-4">
                    <label for="city" class="form-label">Город</label>
                    <label id="city-error" class="error" for="city"></label>
                    <select class="form-control" id="city" name="city">
                        <option value="">Выберите...</option>
                        <?php foreach ($cities as $key => $city) :?>
                            <option value="<?php echo $key?>"><?php echo $city?></option>
                        <?php endforeach;?>
                    </select>
                </div>

                <div class="col-12 mt-4">
                    <label for="good" class="form-label">Товар</label>
                    <label id="good-error" class="error" for="good"></label>
                    <select class="form-control" id="good" name="good">
                        <option value="">Выберите...</option>
                        <?php foreach ($goods as $key => $good) :?>
                            <option value="<?php echo $key?>"><?php echo $good?></option>
                        <?php endforeach;?>
                    </select>
                </div>
            </div>

            <hr class="my-4">

            <button class="w-100 btn btn-primary btn-lg" type="submit">Отправить заявку</button>
        </form>
    </div>
    <div class="col-md-6 col-lg-6 order-md-last">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Последние заявки</span>
        </h4>
        <ul class="list-group mb-3">
            <?php foreach ($orders as $order) :?>
                <li class="list-group-item d-flex justify-content-between lh-sm">
                    <div>
                    <h6 class="my-0"><?php if ($order['name']) echo $order['name']?> <?php if ($order['surname']) echo $order['surname']?></h6>
                    <small class="text-muted"><?php if ($order['email']) echo $order['email']?></small><br>
                    <small class="text-muted"><?php if ($order['city'])  echo $cities[$order['city']] . ', '?><?php if ($order['address']) echo $order['address']?></small><br>
                    </div>
                    <span class="text-muted"><?php if ($order['good']) echo $goods[$order['good']]?></span>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="text-center"><h4><a href="list.php">Все заказы</a></h4></div>
    </div>
</div>
<?php require_once '../footer.php'?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../js/jquery.validate.js"></script>
<script type="text/javascript" src='../js/validation.js'></script>