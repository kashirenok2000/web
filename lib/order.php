<?php
require_once 'dbconn.php';

$cities = [
    'volgograd' => 'Волгоград',
    'moscow' => 'Москва',
    'saratov' => 'Саратов',
    'voronezh' => 'Воронеж',
];

$goods = [
    'clock' => 'Часы',
    'ring' => 'Кольцо',
    'bracer' => 'Браслет',
];

function getOrders($limitOrder = 0)
{
    global $link;
    $limitStr = '';
    if ($limitOrder)
    {
        $limitStr = ' LIMIT ' . $limitOrder;
    }
    $prepCheck = $link->prepare('SELECT * FROM orders ORDER BY id DESC' . $limitStr);
    $prepCheck->execute();
    $orders = [];
    while ($order = $prepCheck->fetch())
    {
        $orders[] = $order;
    }
    return $orders;
}

function addOrder($data)
{
    if ($data)
    {
        global $link;
        $prep = $link->prepare('INSERT INTO orders (name, surname, address, city, email, good) VALUES (:p_name, :p_surname, :p_address, :p_city, :p_email, :p_good);');
        $prep->bindValue(':p_name', $data['name'], PDO::PARAM_STR);
        $prep->bindValue(':p_surname', $data['surname'], PDO::PARAM_STR);
        $prep->bindValue(':p_address', $data['address'], PDO::PARAM_STR);
        $prep->bindValue(':p_city', $data['city'], PDO::PARAM_STR);
        $prep->bindValue(':p_email', $data['email'], PDO::PARAM_STR);
        $prep->bindValue(':p_good', $data['good'], PDO::PARAM_STR);
        $order = $prep->execute();
    }
    return $order;
}