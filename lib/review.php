﻿<?php
$text = '';
if (isset($_POST['review']))
{
    $text = $_POST['review'];
}
$censor = [
    'гуси',
    'лебеди'
];
$textExplode = explode(" ", $text);
$replaced = false;
foreach ($censor as $word)
{
    $replace = str_repeat('#', strlen($word) / 2);
    $textExplode = preg_replace('/^' . $word . '$/', $replace, $textExplode);
    $replaced = true;
}
if ($replaced)
{
    $newText = implode(' ', $textExplode);
}