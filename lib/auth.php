<?php
require_once 'dbconn.php';
$login = '';
$password = '';
$errorAuth = false;
$errorMessage = '';
if (isset($_POST['login']))
{
    $login = $_POST['login'];
    if ($login == '') 
    {
        unset($login);
    }
}
if (isset($_POST['password']))
{
    $password = $_POST['password'];
    if ($password == '') 
    {
        unset($password);
    }
}

$prepCheck = $link->prepare('SELECT * FROM users WHERE login = :p_log');
$prepCheck->bindParam(':p_log', $login, PDO::PARAM_STR, 14);
$prepCheck->execute();
$find = $prepCheck->fetch();

if (empty($find['password']) && strlen($password) > 0) 
{
    $errorAuth = true;
    $errorMessage = 'Неверный логин';
}
elseif (!empty($find['password']))
{
    if ($find['password'] == md5($password))
    {
        session_start();
        $sessid = uniqid($find['login']);
        $_SESSION['login'] = $find['login'];
        $_SESSION['city'] = $find['city'];
        $_SESSION['name'] = $find['name'];
        $_SESSION['sessid'] = $sessid;
        $host = $_SERVER['HTTP_HOST'];
        $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        header('Location: http://' . $host . $uri);
    }
    else
    {
        $errorAuth = true;
        $errorMessage = 'Неверный пароль';
    }
}
?>