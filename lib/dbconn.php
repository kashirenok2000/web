<?php

global $link;

$user = "root";
$password = "";

try {
	$link = new PDO("mysql:host=localhost;dbname=test", $user, $password);
} catch (PDOException $e) {
	echo "Error: " . $e->getMessage();
	die();
}