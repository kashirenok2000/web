<?php
    session_start();
    session_destroy();
    $host = $_SERVER['HTTP_HOST'];
    $uri = rtrim(dirname($_SERVER['PHP_SELF'], 2), '/\\');
    header('Location: http://' . $host . $uri . '\/auth');
?>