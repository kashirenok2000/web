<?php 
require_once '../lib/review.php';
require_once '../header.php';
?>
<div class="text-center">
    <form class="form-signin" method="post" action="">
        <h1 class="h3 mb-3 font-weight-normal">Оставьте ваш отзыв</h1>
        <p>
            <textarea class="form-control" rows="5" cols="30" name="review"><?php echo $text?></textarea>
        </p>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Отправить</button>
    </form>
</div>
<?php if ($replaced) :?>
    <div class="text-center">
        <h1 class="h3 mb-3 font-weight-normal">Ваш отзыв</h1>
        <p>
            <?php echo $newText?>
        </p>
	</div>
<?php endif;?>
<?php require_once '../footer.php'?>